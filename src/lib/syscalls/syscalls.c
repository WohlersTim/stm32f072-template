/*******************************************************************************
 * @file     stdio.c
 * @brief    Implementation of newlib syscall
 ********************************************************************************/

#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>

/**
 * DS: seems, we don't need any of these if we use our own implementation of printf/sprintf
 * XXX: remove
 */
#if 0
int link(char *old, char *new) {
return -1;
}

int _close(int file)
{
  return -1;
}

int _fstat(int file, struct stat *st)
{
  st->st_mode = S_IFCHR;
  return 0;
}

int _isatty(int file)
{
  return 1;
}


int _lseek(int file, int ptr, int dir)
{
  return 0;
}


int _read(int file, char *ptr, int len)
{
  return 0;
}


int _write(int file, char *ptr, int len)
{
  return len;
}


void abort(void)
{
  /* Abort called */
  while(1);
}
#endif
/* --------------------------------- End Of File ------------------------------ */
